// Constants
const GRID_SIZE = 25;
const EMPTY = 0;
const WALL = 1;
const START = 2;

const DIRECTION_DELTAS = {
    up: { row: -1, col: 0 },
    right: { row: 0, col: 1 },
    down: { row: 1, col: 0 },
    left: { row: 0, col: -1 }
};


function isValidCell(row, col) {
    return row >= 0 && row < GRID_SIZE && col >= 0 && col < GRID_SIZE;
}

const END = 3;

// Declare mazeGrid variable
let mazeGrid = [];

// Initialize startCell and endCell variables
let startCell = null;
let endCell = null;




// Track mouse button state
let isMousePressed = false;

let isHoverEnabled = true;

let isDragging = false;


// This is better, Adjust this to work with startCell endCell and already existing cells in     <div id="maze-grid"></div>

function generateMaze() {
    const gridContainer = document.getElementById("maze-grid");

    // Remove existing cells
    while (gridContainer.firstChild) {
        gridContainer.firstChild.remove();
    }

    // Reset mazeGrid
    mazeGrid = [];

    for (let row = 0; row < GRID_SIZE; row++) {
        const rowDiv = document.createElement("div");
        rowDiv.className = "row";

        const rowData = [];

        for (let col = 0; col < GRID_SIZE; col++) {
            const cell = document.createElement("div");
            cell.className = "cell";

            cell.addEventListener("mousedown", () => {
                isMousePressed = true;
                isDragging = false;

                toggleCellType(row, col);
            });
            cell.addEventListener("mouseup", () => {
                isMousePressed = false;
                isDragging = false;

            });
            cell.addEventListener("mouseover", () => {
                if (isMousePressed && isHoverEnabled) {
                    toggleCellType(row, col);
                }
            });
            cell.addEventListener("mousemove", (event) => {
                if (event.buttons === 1) {
                    enforceBlackColor(row, col);
                }
            });
            cell.ondragstart = function (event) {
                event.preventDefault();
            };

            rowDiv.appendChild(cell);
            rowData.push({ type: EMPTY, element: cell });
        }

        gridContainer.appendChild(rowDiv);
        mazeGrid.push(rowData);
    }

    // Reset startCell and endCell
    startCell = null;
    endCell = null;

    // Display the maze matrix
    const mazeState = getMazeState();
    console.log(mazeState);
}
function enforceBlackColor(row, col) {
    const cell = mazeGrid[row][col].element;
    const currentType = mazeGrid[row][col].type;

    if (currentType !== WALL) {
        mazeGrid[row][col].type = WALL;
        cell.style.backgroundColor = "black";
    }
}
function generateHeatmap(distances) {
    const gridContainer = document.getElementById("maze-grid");

    for (let row = 0; row < GRID_SIZE; row++) {
        for (let col = 0; col < GRID_SIZE; col++) {
            const cell = mazeGrid[row][col];
            const distance = distances[row][col];

            // Calculate the color based on the distance value
            const colorValue = Math.floor((1 - distance / GRID_SIZE) * 255);
            const color = `rgb(${colorValue}, ${colorValue}, ${colorValue})`;

            // Set the background color of the cell
            cell.element.style.backgroundColor = color;
        }
    }
}




function toggleCellType(row, col) {
    const cell = mazeGrid[row][col];


    if (cell.type === WALL) {
        cell.type = EMPTY;
        cell.element.className = "cell";
    } else if (cell.type === START) {
        // Do nothing when start cell is clicked
    } else if (cell.type === END) {
        // Do nothing when end cell is clicked
    } else if (!startCell) {
        cell.type = START;
        cell.element.className = "cell start";
        startCell = cell;
    } else if (!endCell) {
        cell.type = END;
        cell.element.className = "cell end";
        endCell = cell;
    } else {
        cell.type = WALL;
        cell.element.className = "cell wall";
    }
}



// Function to check if the cell is a start or end cell
function isStartEndCell(row, col) {
    return (
        (startCell && startCell.row === row && startCell.col === col) ||
        (endCell && endCell.row === row && endCell.col === col)
    );
}





// Function to get the latest maze state
function getMazeState() {
    const mazeState = [];
    for (let row = 0; row < GRID_SIZE; row++) {
        const rowData = [];
        for (let col = 0; col < GRID_SIZE; col++) {
            rowData.push(mazeGrid[row][col].type);
        }
        mazeState.push(rowData);
    }
    return mazeState;
}

// Function to solve the maze using a specified algorithm
function solveMaze(algorithm) {
    if (!startCell || !endCell) {
        alert("Please select a start and end point.");
        return;
    }

    const mazeState = getMazeState();

    let path = [];
    try {
        if (algorithm === "dijkstra") {
            path = dijkstraAlgorithm(mazeState);
        } else if (algorithm === "a-star") {
            path = aStarAlgorithm(mazeState);
        } else if (algorithm === "bfs") {
            path = bfsAlgorithm(mazeState);
        }
    } catch (error) {
        console.error("An error occurred during the solving process:", error);
        alert("An error occurred during the solving process. Please try again.");
    }

    if (path.length > 0) {
        console.log(path.length)
        markPath(path);
    } else {
        alert("No path found!");
    }

}

// Function to solve the maze using Dijkstra's algorithm
function dijkstraAlgorithm(mazeState) {
    const rows = mazeState.length;
    const cols = mazeState[0].length;
    const distances = new Array(rows).fill().map(() => new Array(cols).fill(Infinity));
    const previous = new Array(rows).fill().map(() => new Array(cols).fill(null));

    const queue = [];

    const startRow = mazeGrid.findIndex((row) => row.includes(startCell));
    const startCol = mazeGrid[startRow].findIndex((cell) => cell === startCell);
    const endRow = mazeGrid.findIndex((row) => row.includes(endCell));
    const endCol = mazeGrid[endRow].findIndex((cell) => cell === endCell);

    distances[startRow][startCol] = 0;
    queue.push({ row: startRow, col: startCol });

    while (queue.length > 0) {
        const { row, col } = queue.shift();

        if (row === endRow && col === endCol) {
            break;
        }

        const neighbors = getNeighbors(row, col, rows, cols);

        for (const neighbor of neighbors) {
            const { neighborRow, neighborCol } = neighbor;

            if (mazeState[neighborRow][neighborCol] !== WALL && distances[row][col] + 1 < distances[neighborRow][neighborCol]) {
                distances[neighborRow][neighborCol] = distances[row][col] + 1;
                previous[neighborRow][neighborCol] = { row, col };
                queue.push({ row: neighborRow, col: neighborCol });
            }
        }
    }

    const path = [];
    let currentRow = endRow;
    let currentCol = endCol;

    while (previous[currentRow][currentCol]) {
        path.unshift({ row: currentRow, col: currentCol });
        const { row, col } = previous[currentRow][currentCol];
        currentRow = row;
        currentCol = col;
    }

    return path;
}

// Function to solve the maze using A* search algorithm
function aStarAlgorithm(mazeState) {
    const rows = mazeState.length;
    const cols = mazeState[0].length;
    const distances = new Array(rows).fill().map(() => new Array(cols).fill(Infinity));
    const previous = new Array(rows).fill().map(() => new Array(cols).fill(null));

    const queue = [];

    const startRow = mazeGrid.findIndex((row) => row.includes(startCell));
    const startCol = mazeGrid[startRow].findIndex((cell) => cell === startCell);
    const endRow = mazeGrid.findIndex((row) => row.includes(endCell));
    const endCol = mazeGrid[endRow].findIndex((cell) => cell === endCell);

    distances[startRow][startCol] = 0;
    queue.push({ row: startRow, col: startCol });

    while (queue.length > 0) {
        const { row, col } = queue.shift();

        if (row === endRow && col === endCol) {
            break;
        }

        const neighbors = getNeighbors(row, col, rows, cols);

        for (const neighbor of neighbors) {
            const { neighborRow, neighborCol } = neighbor;

            if (mazeState[neighborRow][neighborCol] !== WALL && distances[row][col] + 1 < distances[neighborRow][neighborCol]) {
                distances[neighborRow][neighborCol] = distances[row][col] + 1;
                previous[neighborRow][neighborCol] = { row, col };
                queue.push({ row: neighborRow, col: neighborCol });
            }
        }
    }

    // Check if a path was found
    if (previous[endRow][endCol] === null) {
        return []; // No path found
    }

    const path = [];
    let currentRow = endRow;
    let currentCol = endCol;

    while (previous[currentRow][currentCol]) {
        path.unshift({ row: currentRow, col: currentCol });
        const { row, col } = previous[currentRow][currentCol];
        currentRow = row;
        currentCol = col;
    }

    return path;
}

// Function to solve the maze using Breadth-First Search (BFS) algorithm
// Function to solve the maze using Breadth-First Search (BFS) algorithm
function bfsAlgorithm(mazeState) {
    const rows = mazeState.length;
    const cols = mazeState[0].length;
    const distances = new Array(rows).fill().map(() => new Array(cols).fill(Infinity));
    const previous = new Array(rows).fill().map(() => new Array(cols).fill(null));
    const startRow = mazeGrid.findIndex((row) => row.some((cell) => cell.type === START));
    const startCol = mazeGrid[startRow].findIndex((cell) => cell.type === START);
    const endRow = mazeGrid.findIndex((row) => row.some((cell) => cell.type === END));
    const endCol = mazeGrid[endRow].findIndex((cell) => cell.type === END);

    // Initialize distances and previous arrays
    distances[startRow][startCol] = 0;

    const visited = new Array(rows).fill().map(() => new Array(cols).fill(false));
    const queue = [];

    visited[startRow][startCol] = true;

    queue.push({ row: startRow, col: startCol, path: [] });

    while (queue.length > 0) {
        const { row, col, path } = queue.shift();

        if (row === endRow && col === endCol) {
            return path;
        }

        const neighbors = getNeighbors(row, col, rows, cols);

        for (const neighbor of neighbors) {
            const { neighborRow, neighborCol } = neighbor;

            if (!visited[neighborRow][neighborCol] && mazeState[neighborRow][neighborCol] !== WALL) {
                visited[neighborRow][neighborCol] = true;

                distances[neighborRow][neighborCol] = distances[row][col] + 1;
                previous[neighborRow][neighborCol] = { row, col };

                queue.push({
                    row: neighborRow,
                    col: neighborCol,
                    path: [...path, { row: neighborRow, col: neighborCol }],
                });
            }
        }
    }

    return [];
}





// Function to calculate the heuristic (Manhattan distance) between two cells
function heuristic(row1, col1, row2, col2) {
    return Math.abs(row1 - row2) + Math.abs(col1 - col2);
}

// Function to reconstruct the path from the start cell to the given cell
function reconstructPath(cameFrom, cell) {
    const path = [];
    while (cell) {
        path.unshift(cell);
        cell = cameFrom[cell];
    }
    return path;
}

// Helper function to get the coordinates (row, col) of a cell
function getCellCoordinates(cell) {
    for (let row = 0; row < GRID_SIZE; row++) {
        for (let col = 0; col < GRID_SIZE; col++) {
            if (mazeGrid[row][col] === cell) {
                return { row, col };
            }
        }
    }
    return null;
}

// Helper function to get valid neighboring cells
function getNeighbors(row, col, rows, cols) {
    const neighbors = [];

    if (row > 0) {
        neighbors.push({ neighborRow: row - 1, neighborCol: col });
    }
    if (row < rows - 1) {
        neighbors.push({ neighborRow: row + 1, neighborCol: col });
    }
    if (col > 0) {
        neighbors.push({ neighborRow: row, neighborCol: col - 1 });
    }
    if (col < cols - 1) {
        neighbors.push({ neighborRow: row, neighborCol: col + 1 });
    }

    return neighbors;
}


// Function to mark the path on the maze grid
function markPath(path) {
    // Clear only the cells that are not obstacles
    for (let row = 0; row < GRID_SIZE; row++) {
        for (let col = 0; col < GRID_SIZE; col++) {
            const cell = mazeGrid[row][col];
            if (cell.type !== START && cell.type !== END && cell.type !== WALL) {
                cell.element.className = "cell";
            }
        }
    }

    // Mark the new path
    for (const { row, col } of path) {
        const cell = mazeGrid[row][col];
        if (cell.type !== START && cell.type !== END && cell.type !== WALL) {
            cell.element.className = "cell path";
        }
    }
}

function generateRandomMazeBacktracking() {
    // Clear existing maze cells
    const gridContainer = document.getElementById("maze-grid");
    const mazeCells = gridContainer.getElementsByClassName("cell");
    while (mazeCells.length > 0) {
        mazeCells[0].parentNode.removeChild(mazeCells[0]);
    }

    mazeGrid = [];

    for (let row = 0; row < GRID_SIZE; row++) {
        const rowDiv = document.createElement("div");
        rowDiv.className = "row";

        const rowData = [];

        for (let col = 0; col < GRID_SIZE; col++) {
            const cell = document.createElement("div");
            cell.className = "cell";

            cell.addEventListener("mousedown", () => {
                isMousePressed = true;
                isDragging = false;

                toggleCellType(row, col);
            });
            cell.addEventListener("mouseup", () => {
                isMousePressed = false;
                isDragging = false;
            });
            cell.addEventListener("click", () => {
                toggleCellType(row, col);
            });
            cell.addEventListener("mousemove", (event) => {
                if (event.buttons === 1) {
                    enforceBlackColor(row, col);
                }
            });
            cell.addEventListener("mouseover", () => {
                if (isMousePressed && !isDragging && isHoverEnabled) {
                    toggleCellType(row, col);
                }
            });

            rowDiv.appendChild(cell);
            rowData.push({ type: EMPTY, element: cell });
        }

        gridContainer.appendChild(rowDiv);
        mazeGrid.push(rowData);
    }

    let start, end;
    if (startCell && endCell) {
        // Use existing start and end cells
        start = { row: startCell.row, col: startCell.col };
        end = { row: endCell.row, col: endCell.col };
    } else {
        // Select random start and end points
        start = { row: Math.floor(Math.random() * GRID_SIZE), col: Math.floor(Math.random() * GRID_SIZE) };
        end = { row: Math.floor(Math.random() * GRID_SIZE), col: Math.floor(Math.random() * GRID_SIZE) };

        // Render the start and end cells to the UI
        toggleCellType(start.row, start.col, START);
        toggleCellType(end.row, end.col, END);
    }

    // Generate the maze using backtracking algorithm
    carvePath(start.row, start.col);

    // Display the maze matrix
    const mazeState = getMazeState();
    console.log(mazeState);
}



function carvePath(row, col, endRow, endCol) {
    const directions = ["up", "right", "down", "left"];
    shuffleArray(directions);

    for (let i = 0; i < directions.length; i++) {
        const dir = directions[i];
        const newRow = row + DIRECTION_DELTAS[dir].row;
        const newCol = col + DIRECTION_DELTAS[dir].col;

        if (
            isValidCell(newRow, newCol) &&
            mazeGrid[newRow][newCol].type === WALL
        ) {
            mazeGrid[row][col].element.classList.remove("wall");
            mazeGrid[newRow][newCol].element.classList.remove("wall");

            if (newRow === endRow && newCol === endCol) {
                return;
            }

            // Mark the current cell and neighboring cell as visited
            mazeGrid[row][col].type = EMPTY;
            mazeGrid[row][col].element.className = "cell";
            mazeGrid[newRow][newCol].type = EMPTY;
            mazeGrid[newRow][newCol].element.className = "cell";

            carvePath(newRow, newCol, endRow, endCol);
        }
    }
}

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}
// Generate the maze on page load
generateMaze();
